
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button'
import { Link } from 'react-router-dom'

export default function ProductCard ({courseProp}) {

const { name, description, price, _id } = courseProp;
  return (
  <Card>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
      <Card.Text>{price}</Card.Text>

        <Button variant="primary" as={Link} to={`/Productview/${_id}`}>Details</Button>
      </Card.Body>
    </Card>
  )
}