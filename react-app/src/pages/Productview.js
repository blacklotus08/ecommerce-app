import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams } from 'react-router-dom'
import UserContext from '../userContext';
import { useNavigate, Link } from 'react-router-dom'
import Swal from 'sweetalert2'

export default function Productview() {

	// We use curly brackets {} to tell the computer that we are trying to use the user variable from the userContext in App.js and store it in 'user' variable in Courseview.js. Curly brackets is also use if the data is from a different page
			const { user } = useContext(UserContext);
			const { productId } = useParams();

			const navigate = useNavigate()

			const [name, setName] = useState("");
			const [description, setDescription] = useState("");
			const [price, setPrice] = useState(0);

			useEffect(() => {

				console.log(productId);

				fetch(`http://localhost:4000/products/${productId}`)
				.then(res => res.json())
				.then(data => {

					setName(data.name);
					setDescription(data.description);
					setPrice(data.price);
				})
			})

			const buy = (productId) => {
				fetch(`http://localhost:4000/users/addToCart`, {
					method: "POST",
					headers: {
						'Content-Type': 'application/json',
						Authorization: `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
						productId: productId
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)

					if(data === true) {
						Swal.fire({
							title: "Successfully Added!",
							icon: "success",
							text: "You have successfully added this product!"
						})

						navigate("/products");
					} else {
						Swal.fire({
							title: "Something went wrong!",
							icon: "error",
							text: "Please try again!"
						})
					}
				})
			}

			return(
				<Container className="mt-5">
					<Row>
						<Col lg={{ span: 6, offset: 3 }}>
							<Card>
								<Card.Body className="text-center">
									<Card.Title>{name}</Card.Title>
									<Card.Subtitle>Description:</Card.Subtitle>
									<Card.Text>{description}</Card.Text>
									<Card.Subtitle>Price:</Card.Subtitle>
									<Card.Text>PhP {price}</Card.Text>
					
									{
										(user.id !== null)?
									
									<Button variant="primary" block onClick={() => buy(productId)}>Add to Cart</Button>
									:
									<Link className="btn btn-danger btn-block" to="/login">LogIn</Link>
									}
								</Card.Body>		
							</Card>
						</Col>
					</Row>
				</Container>
			)
		}