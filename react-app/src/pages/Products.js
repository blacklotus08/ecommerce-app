import { useState, useEffect } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import ProductCard from "../components/ProductCard";
import React from "react";

export default function Products() {
  const [products, setProducts] = useState([]);
  const [show, setShow] = useState(false);
  const [disabledButton, setDisabledFormButton] = useState(true);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const initialFormData = Object.freeze({
    name: "",
    description: "",
    price: 1,
  });
  const [formData, updateFormData] = React.useState(initialFormData);

  const handleChange = (e) => {
    updateFormData({
      ...formData,

      // Trimming any whitespace
      [e.target.name]: e.target.value.trim(),
    });

    if (
      formData.name !== "" &&
      formData.description !== "" &&
      formData.price !== 0
    ) {
      setDisabledFormButton(false);
    }
  };

  const fetchProductList = () => {
	fetch("http://localhost:4000/products/activeProducts")
	.then((res) => res.json())
	.then((data) => {
	  setProducts(data);
	});
  }

  useEffect(() => {
	fetchProductList();
  }, []);

  const handleAddProduct = () => {
    handleShow();
  };

  const submitForm = () => {
    handleClose();
    fetch("http://localhost:4000/products/create", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log("Product added:", data);
        // Update the products list with the new product
		fetchProductList();
      })
      .catch((error) => {
        console.error("Error adding product:", error);
      });
  };

  return (
    <div>
      <div className="d-flex justify-content-between mb-2">
        <h3>Products</h3>
        <Button variant="primary" onClick={handleAddProduct}>
          Add Product
        </Button>
      </div>
      {products.map((product) => {
        return <ProductCard key={product._id} courseProp={product} />;
      })}

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Add New Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {
            <Form>
              <Form.Group className="mb-3">
                <Form.Label>Name</Form.Label>
                <Form.Control
                  type="text"
                  name="name"
                  autoFocus
                  required
                  onChange={handleChange}
                />
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>Description</Form.Label>
                <Form.Control
                  type="text"
                  name="description"
                  required
                  onChange={handleChange}
                />
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>Price</Form.Label>
                <Form.Control
                  type="number"
                  name="price"
                  required
                  onChange={handleChange}
                />
              </Form.Group>
            </Form>
          }
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="primary"
            onClick={submitForm}
            disabled={disabledButton}
          >
            Add
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}
